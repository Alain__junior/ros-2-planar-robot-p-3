# ros2_planar_robot

## November $21^{st}$ 2023

### (a) Develop a `kinematic_model` node complying with the following characteristics:

- Subscribed to a topic `/q`, receiving messages with type `JointPosition`. This type should have a vector field of size 2;

- Publisher in a topic named `/kin_data`, in which the cartesian pose and jacobian matrix are published every time a joint position is received (except from the case described below);

- Has parameters `l1` and `l2`, with default value of `0`. In case these parameters are not initialized by the user, the no data should be published.

### (b) Develop a `controller` node complying with the following characteristics:

- Subscribed to `/desired_state`, in order to read the desired cartesian position and velocity;

- Subscribed to `/kin_data`, in order to read the actual cartesian position and the jacobian matrix;

- Publisher of the desired feedback joint velocities, on topic `/Dqd`;

- Should publish null desired joint velocities if the node is created before the `/kin_data` topic is active.

### (c) Develop a `simulator` node complying with the following characteristics:

- Subscribed to a topic named `/Dqd`, reading the desired joint velocity;

- Subscribed to a topic named `/Dq_dist`, reading the joint velocity disturbances;

- Streams the next joint positions in `/q`, integrating the variables described above.

### (d) Develop a `disturbance` node complying with the following characteristics:

- Streams the joint velocities disturbances in `/Dq_dist`, following an expressions such as $\dot{q}_{dist}=A\,\sin(\omega\,t)$, with $A$ and $\omega$ parameters of the node.



------------------------------------------------------------------------------------

### Answers

#### 1. Mention a node in which a variable is read/written within different callback functions:

#### 2. Does it lead to issues related to functions sharing memory? Discuss in detail how this is dealt with.

#### 3. Discuss the differences between what you achieved in the previous commit and this current commit:

I couldn't implement the `trajectory_generator` because...

#### 4. Write here the instructions to demonstrate the functionality of the developed solutions:

1st terminal:
```
cd ros_workspace
source /opt/ros/...
colcon build
ros2 run ros2_planar_robot ...
```

2nd terminal:
```
ros2 topic echo etc...
```

3rd terminal:
```
ros2 topic pub etc...
```
------------------------------------------------------------------------------------

### Updating your forked directory

You can update your forked repository adding this project as a remote, fetching the updated commits and merging it with your current local:

```
cd ros_workspace/src/ros2_planar_robot
git remote add prof git@gitlab.com:joao.cv/ros2_planar_robot.git
git fetch assignment_remote
git merge assignment_remote/main
```
You will probably need to solve the conflicts, selecting the desired changes between your local and this remote. You can use `vscode` for that.

------------------------------------------------------------------------------------