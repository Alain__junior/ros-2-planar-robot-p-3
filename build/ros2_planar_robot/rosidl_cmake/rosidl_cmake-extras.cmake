# generated from rosidl_cmake/cmake/rosidl_cmake-extras.cmake.in

set(ros2_planar_robot_IDL_FILES "msg/RefPose.idl;msg/Pose.idl;msg/CartesianState.idl;msg/Velocity.idl;msg/KinData.idl;msg/JointPosition.idl")
set(ros2_planar_robot_INTERFACE_FILES "msg/RefPose.msg;msg/Pose.msg;msg/CartesianState.msg;msg/Velocity.msg;msg/KinData.msg;msg/JointPosition.msg")
