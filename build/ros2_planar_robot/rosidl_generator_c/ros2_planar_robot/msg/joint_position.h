// generated from rosidl_generator_c/resource/idl.h.em
// with input from ros2_planar_robot:msg/JointPosition.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__JOINT_POSITION_H_
#define ROS2_PLANAR_ROBOT__MSG__JOINT_POSITION_H_

#include "ros2_planar_robot/msg/detail/joint_position__struct.h"
#include "ros2_planar_robot/msg/detail/joint_position__functions.h"
#include "ros2_planar_robot/msg/detail/joint_position__type_support.h"

#endif  // ROS2_PLANAR_ROBOT__MSG__JOINT_POSITION_H_
