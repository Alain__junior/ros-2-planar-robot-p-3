// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from ros2_planar_robot:msg/JointPosition.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_POSITION__BUILDER_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_POSITION__BUILDER_HPP_

#include <algorithm>
#include <utility>

#include "ros2_planar_robot/msg/detail/joint_position__struct.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


namespace ros2_planar_robot
{

namespace msg
{

namespace builder
{

class Init_JointPosition_theta2
{
public:
  explicit Init_JointPosition_theta2(::ros2_planar_robot::msg::JointPosition & msg)
  : msg_(msg)
  {}
  ::ros2_planar_robot::msg::JointPosition theta2(::ros2_planar_robot::msg::JointPosition::_theta2_type arg)
  {
    msg_.theta2 = std::move(arg);
    return std::move(msg_);
  }

private:
  ::ros2_planar_robot::msg::JointPosition msg_;
};

class Init_JointPosition_theta1
{
public:
  Init_JointPosition_theta1()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_JointPosition_theta2 theta1(::ros2_planar_robot::msg::JointPosition::_theta1_type arg)
  {
    msg_.theta1 = std::move(arg);
    return Init_JointPosition_theta2(msg_);
  }

private:
  ::ros2_planar_robot::msg::JointPosition msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::ros2_planar_robot::msg::JointPosition>()
{
  return ros2_planar_robot::msg::builder::Init_JointPosition_theta1();
}

}  // namespace ros2_planar_robot

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_POSITION__BUILDER_HPP_
