// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from ros2_planar_robot:msg/KinData.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__KIN_DATA__BUILDER_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__KIN_DATA__BUILDER_HPP_

#include <algorithm>
#include <utility>

#include "ros2_planar_robot/msg/detail/kin_data__struct.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


namespace ros2_planar_robot
{

namespace msg
{

namespace builder
{

class Init_KinData_jacobian
{
public:
  explicit Init_KinData_jacobian(::ros2_planar_robot::msg::KinData & msg)
  : msg_(msg)
  {}
  ::ros2_planar_robot::msg::KinData jacobian(::ros2_planar_robot::msg::KinData::_jacobian_type arg)
  {
    msg_.jacobian = std::move(arg);
    return std::move(msg_);
  }

private:
  ::ros2_planar_robot::msg::KinData msg_;
};

class Init_KinData_pose
{
public:
  Init_KinData_pose()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_KinData_jacobian pose(::ros2_planar_robot::msg::KinData::_pose_type arg)
  {
    msg_.pose = std::move(arg);
    return Init_KinData_jacobian(msg_);
  }

private:
  ::ros2_planar_robot::msg::KinData msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::ros2_planar_robot::msg::KinData>()
{
  return ros2_planar_robot::msg::builder::Init_KinData_pose();
}

}  // namespace ros2_planar_robot

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__KIN_DATA__BUILDER_HPP_
