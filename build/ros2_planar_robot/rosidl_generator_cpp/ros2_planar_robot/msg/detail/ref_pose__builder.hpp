// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from ros2_planar_robot:msg/RefPose.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__REF_POSE__BUILDER_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__REF_POSE__BUILDER_HPP_

#include <algorithm>
#include <utility>

#include "ros2_planar_robot/msg/detail/ref_pose__struct.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


namespace ros2_planar_robot
{

namespace msg
{

namespace builder
{

class Init_RefPose_deltat
{
public:
  explicit Init_RefPose_deltat(::ros2_planar_robot::msg::RefPose & msg)
  : msg_(msg)
  {}
  ::ros2_planar_robot::msg::RefPose deltat(::ros2_planar_robot::msg::RefPose::_deltat_type arg)
  {
    msg_.deltat = std::move(arg);
    return std::move(msg_);
  }

private:
  ::ros2_planar_robot::msg::RefPose msg_;
};

class Init_RefPose_y
{
public:
  explicit Init_RefPose_y(::ros2_planar_robot::msg::RefPose & msg)
  : msg_(msg)
  {}
  Init_RefPose_deltat y(::ros2_planar_robot::msg::RefPose::_y_type arg)
  {
    msg_.y = std::move(arg);
    return Init_RefPose_deltat(msg_);
  }

private:
  ::ros2_planar_robot::msg::RefPose msg_;
};

class Init_RefPose_x
{
public:
  Init_RefPose_x()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_RefPose_y x(::ros2_planar_robot::msg::RefPose::_x_type arg)
  {
    msg_.x = std::move(arg);
    return Init_RefPose_y(msg_);
  }

private:
  ::ros2_planar_robot::msg::RefPose msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::ros2_planar_robot::msg::RefPose>()
{
  return ros2_planar_robot::msg::builder::Init_RefPose_x();
}

}  // namespace ros2_planar_robot

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__REF_POSE__BUILDER_HPP_
