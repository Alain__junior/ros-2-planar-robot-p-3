#include <chrono> // Date and time
#include <functional> // Arithmetic, comparisons, and logical operations
#include <memory> // Dynamic memory management
#include <string> // String functions
#include "geometry_msgs/msg/twist.hpp"
#include "sensor_msgs/msg/joint_state.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "ros2_planar_robot/msg/cartesian_state.hpp"
#include "ros2_planar_robot/msg/kin_data.hpp"
#include "ros2_planar_robot/msg/velocity.hpp"
#include "control_lib.h"

using namespace std::chrono_literals;
using std::placeholders::_1;

// Controller node, computing the feedback desired joint velocity
class ControlNode : public rclcpp::Node
{
  public:
    ControlNode():
      Node("controller")
      {      
        subs_kin_data_ = this->create_subscription <ros2_planar_robot::msg::KinData>   (
            "kin_data", 10, std::bind(&ControlNode::kin_callback, this, _1)
        );
        // Subscription to the launch node, used to launch the whole system
        subs_des_data_  = this->create_subscription <ros2_planar_robot::msg::CartesianState>   (
            "desired_state", 10, std::bind(&ControlNode::des_callback, this, _1)
        );
        pub_vel_ = this->create_publisher    <ros2_planar_robot::msg::Velocity>(
            "Dqt",10);
      };


  private:
  void kin_callback(const ros2_planar_robot::msg::KinData::SharedPtr kin_data)
  {
    x_actual_<<kin_data->pose.x,kin_data->pose.y;
    J<< kin_data->jacobian[0],kin_data->jacobian[1],
                  kin_data->jacobian[2],kin_data->jacobian[3];
    running_=true;
    
    
  }
  void des_callback(const ros2_planar_robot::msg::CartesianState::SharedPtr data_des )
  {
    x_desired_<<data_des->pose.x,data_des->pose.y;
    dxd_ << data_des->velocity.dx,data_des->velocity.dy;
    //timer_callback();
    timer_ = this->create_wall_timer(
          1ms, std::bind(&ControlNode::timer_callback, this));
    
  }
  void timer_callback()
  {
    auto message=ros2_planar_robot::msg::Velocity();
    Controller control;
     if (J==zero){
      message.dx=0.0;
      message.dy=0.0;
      pub_vel_->publish(message);

    }
    else {
    dq_des=control.Dqd(x_actual_,x_desired_,dxd_,J);
    message.dx=dq_des(0);
    message.dy=dq_des(1);
    pub_vel_->publish(message);
    }
    
    
    
  }


















Eigen::Vector2d x_desired_     { Eigen::Vector2d::Zero() } ;
Eigen::Vector2d x_actual_ { Eigen::Vector2d::Zero() } ;
Eigen::Vector2d dxd_     { Eigen::Vector2d::Zero() } ;
Eigen::Vector2d dq_des     { Eigen::Vector2d::Zero() } ;
Eigen::Matrix2d J           { Eigen::Matrix2d::Zero() };
Eigen::Matrix2d zero { Eigen::Matrix2d::Zero() } ;
bool          running_  {false};
rclcpp::TimerBase::SharedPtr                                          timer_                                          ;

rclcpp::Publisher<ros2_planar_robot::msg::Velocity>::SharedPtr  pub_vel_                               ;   
rclcpp::Subscription<ros2_planar_robot::msg::KinData>::SharedPtr      subs_kin_data_                                 ;  
rclcpp::Subscription<ros2_planar_robot::msg::CartesianState>::SharedPtr  subs_des_data_                               ;     
};
 
int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<ControlNode>());  
  rclcpp::shutdown();
  return 0;
}