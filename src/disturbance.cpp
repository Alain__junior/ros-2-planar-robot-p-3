// disturbance.cpp
#include <chrono> // Date and time
//#include <eigen3/Eigen/src/Core/GlobalFunctions.h>
//#include <eigen3/Eigen/src/Core/MathFunctions.h>
#include <functional> // Arithmetic, comparisons, and logical operations
#include <memory> // Dynamic memory management
#include <string> // String functions
#include "geometry_msgs/msg/twist.hpp"
#include "sensor_msgs/msg/joint_state.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "ros2_planar_robot/msg/velocity.hpp"
#include "control_lib.h"
#include <cmath>


using namespace std::chrono_literals;
using std::placeholders::_1;

// Disturbance node
class Disturbance : public rclcpp::Node
{
  public:
    Disturbance():
      Node("pert")
      {      
        pub_dq_dist_ = this->create_publisher    <ros2_planar_robot::msg::Velocity>(
            "Dq_dist",10);
        timer_ = this->create_wall_timer(
        500ms, std::bind(&Disturbance::timer_callback, this));
        this->declare_parameter("A1", 0.0);
        this->declare_parameter("A2", 0.0);
        this->declare_parameter("omega", 0.0);
      }
  private:
    void timer_callback()
    {
      auto message=ros2_planar_robot::msg::Velocity();
      double A1 = this->get_parameter("A1").as_double();
      double  A2 = this->get_parameter("A2").as_double();
      double  omega = this->get_parameter("omega").as_double();
      t = get_clock()->now().seconds();
    
      message.dx= A1*std::sin(omega*t);
      message.dy=A2*std::sin(omega*t);
      pub_dq_dist_->publish(message);
    }
    
float t;
rclcpp::TimerBase::SharedPtr                                          timer_                                          ;

rclcpp::Publisher<ros2_planar_robot::msg::Velocity>::SharedPtr  pub_dq_dist_                               ;   
   

    
      };
  
 
int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<Disturbance>());  
  rclcpp::shutdown();
  return 0;
}