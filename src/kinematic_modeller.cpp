#include <chrono> // Date and time
#include <functional> // Arithmetic, comparisons, and logical operations
#include <memory> // Dynamic memory management
#include <string> // String functions
#include "geometry_msgs/msg/twist.hpp"
#include "sensor_msgs/msg/joint_state.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "kinematic_model_lib.h"
#include "ros2_planar_robot/msg/joint_position.hpp"
#include "ros2_planar_robot/msg/kin_data.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;

// Node able to compute cartesian position and jacobian matrix for a given joint position 
class KinematicModel : public rclcpp::Node
{
  public:
    KinematicModel():
      Node("kinematic_model")
      {   
      subs_joint_position_ = this->create_subscription <ros2_planar_robot::msg::JointPosition>   (
            "q", 10, std::bind(&KinematicModel::topic_callback, this, _1) );
      pub_position_=this->create_publisher    <ros2_planar_robot::msg::KinData>(
            "kin_data",10
        );  
      this->declare_parameter("l1", 0.0);
      this->declare_parameter("l2", 0.0);
      }

  private:
  void topic_callback(const ros2_planar_robot::msg::JointPosition::SharedPtr theta)
  {
    q<< theta->theta1,theta->theta2;
    timer_callback();
    
    

    
  }
  void timer_callback()
  {

    auto message=ros2_planar_robot::msg::KinData();
    l1 = this->get_parameter("l1").as_double();
    l2 = this->get_parameter("l2").as_double();
    if (l1==0 && l2==0)
    {
        RCLCPP_WARN(get_logger(), "Parameters l1 and l2 are not initialized.");

    }
    else {
    RobotModel Rob(l1, l2);
    Rob.FwdKin(X,J,q);
    message.pose.x=X(0);
    message.pose.y=X(1);
    message.jacobian[0]=J(0,0);
    message.jacobian[1]=J(0,1);
    message.jacobian[2]=J(1,0);
    message.jacobian[3]=J(1,1);
    pub_position_->publish(message);
    }
  

  }



double l1;
double l2;
Eigen::Vector2d   q  {Eigen::Vector2d::Zero()};
Eigen::Matrix2d J;
Eigen::Vector2d   X  {Eigen::Vector2d::Zero()};
bool          running_            { false                   } ;
rclcpp::TimerBase::SharedPtr                                          timer_                                          ;
rclcpp::Publisher<ros2_planar_robot::msg::KinData>    ::SharedPtr  pub_position_    ;       
rclcpp::Subscription<ros2_planar_robot::msg::JointPosition> ::SharedPtr  subs_joint_position_    ;  
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);  
  rclcpp::spin(std::make_shared<KinematicModel>());
  rclcpp::shutdown();
  return 0;
}