#include <chrono> // Date and time
#include <functional> // Arithmetic, comparisons, and logical operations
#include <memory> // Dynamic memory management
#include <string> // String functions
#include "geometry_msgs/msg/twist.hpp"
#include "sensor_msgs/msg/joint_state.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "ros2_planar_robot/msg/velocity.hpp"
#include "ros2_planar_robot/msg/joint_position.hpp"
#include "control_lib.h"



using namespace std::chrono_literals;
using std::placeholders::_1;

// Simulator node, computing next joint position based on the desired joint velocity, disturbance and previous joint position
class Simulator : public rclcpp::Node
{
  public:
    Simulator():
      Node("sim")
      {     
        subs_vel_dist_ = this->create_subscription <ros2_planar_robot::msg::Velocity>   (
            "Dq_dist", 10, std::bind(&Simulator::vel_dist_callback, this, _1)
        );
        // Subscription to the launch node, used to launch the whole system
        subs_joint_vel_  = this->create_subscription <ros2_planar_robot::msg::Velocity>   (
            "Dqt", 10, std::bind(&Simulator::joint_vel_callback, this, _1)
        );
        pub_pos_ = this->create_publisher    <ros2_planar_robot::msg::JointPosition>(
            "q",10); 
      }
  private:
    void vel_dist_callback (const ros2_planar_robot::msg::Velocity::SharedPtr dq_vel_dist)
    {
      dq_dist_  <<dq_vel_dist->dx,dq_vel_dist->dy;
    }
    void joint_vel_callback(const ros2_planar_robot::msg::Velocity::SharedPtr dq_vel)
    {
      dq_  <<dq_vel->dx,dq_vel->dy ;
      timer_ = this->create_wall_timer(
      500ms, std::bind(&Simulator::timer_callback, this));
    }
    void timer_callback()
    {
      auto message=ros2_planar_robot::msg::JointPosition();
      //t = get_clock()->now().seconds();
      double dt=0.08;
      message.theta1=q_previous_(0)+(dq_(0)+dq_dist_(0))*dt;
      message.theta2=q_previous_(1)+(dq_(1)+dq_dist_(1))*dt;
      q_previous_<<message.theta1,message.theta2;
      pub_pos_->publish(message);
    }

  float t;
  rclcpp::TimerBase::SharedPtr                                          timer_                                          ;
  rclcpp::Publisher<ros2_planar_robot::msg::JointPosition>::SharedPtr  pub_pos_                               ;   
  rclcpp::Subscription<ros2_planar_robot::msg::Velocity>::SharedPtr      subs_joint_vel_                                 ;  
  rclcpp::Subscription<ros2_planar_robot::msg::Velocity>::SharedPtr      subs_vel_dist_                                 ;  
  //Eigen::Vector2d q_next_     { Eigen::Vector2d::Zero() } ;
  Eigen::Vector2d q_previous_ { Eigen::Vector2d::Zero() } ;
  Eigen::Vector2d dq_     { Eigen::Vector2d::Zero() } ;
  Eigen::Vector2d dq_dist_ { Eigen::Vector2d::Zero() } ;
};
 
int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<Simulator>());  
  rclcpp::shutdown();
  return 0;
}